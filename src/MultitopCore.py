# TODO: write here about this file

import os
import threading
from time import sleep

class Multitop(threading.Thread):
    '''Multi-threaded top'''

    # FIXME: these global attributes are really necessary?
    global m, p, u, pid, freq, cpumem, counter, procname

    def pids(self):
        # /proc/stat
        # This file contains the kernel/system statistics

        try:
            statFile = open('/proc/stat')

        except:
            return 'Coconuts are tropical! [ Monty Python ]'

        i = 0
        stat = {}
        for line in statFile.readlines():
            lineArray = line.split(' ')
            stat[lineArray[0]] = lineArray[1:]
            i += 1

        statFile.close()

        for s in stat:
            if s == 'processes':
                numProcesses = stat[s][0]

        for pid in range(0, int(numProcesses)):
            try:
                filename = "/proc/" + str(pid) + "/status"
                file = open(filename)
                print(file.read())
                file.close()

            except:
                continue

        return 'Something someday!'


    def get_info_pid(self, pid):

        try:
            file = open('/proc/' + str(pid) + '/status')
            data = file.read().split()
            file.close()
        except:
            return "That's enough music for now, lads... looks like there's dirty work afoot."

        return str(data[1])


    def ram_pid(self, pid):
    # Bohrer: Funcao OK #

        try:
            file = open('/proc/' + str(pid) + '/statm')
            ram_pid = file.read().split()
            file.close()

        except:
            return "It's just a flesh wound [ Monty Python ]"

        pagesize = os.sysconf("SC_PAGE_SIZE") / 1024
        used_ram = (float(ram_pid[1]) * pagesize / 1024)

        try:
            file = open('/proc/meminfo')
            ram_total = file.read().split()
            file.close()

        except:
            return 'What are you gonna do, bleed on me? [ Monty Python ]'

        conversion_factor = 1024 # 1024KB -> 1 MB
        mem_total = (int(ram_total[1]) / conversion_factor)
        used_ram_percent = float(used_ram * 100 / mem_total)
        return str(str("%2.2f" % used_ram) + ' MB' + ' - ' + str("%2.2f" % used_ram_percent) + '% RAM')


    def proc_pid(self, pid, tts):
    # Bohrer: Funcao OK #

        try:
            file = open('/proc/uptime')
            total_proc = file.read().split()[0].split('.')
            processor_load_all = str(total_proc[0]) + str(total_proc[1])
            file.close()

        except:
            return 'What else floats in water? A Duck! [ Monty Python ]'

        processor_load_t1 = float(processor_load_all)

        try:
            file = open('/proc/' + str(pid) + '/stat')
            pid_proc = file.read().split()
            processor_load_pid = int(pid_proc[13]) + int(pid_proc[14])
            file.close()

        except:
            return 'We are the Knights who say... NI! [ Monty Python ]'

        pid_load_t1 = float(processor_load_pid)

        try:
            sleep(tts)
        except:
            return 'What\'s your favorite colour?'

        try:
            file = open('/proc/uptime')
            total_proc = file.read().split()[0].split('.')
            processor_load_all = str(total_proc[0]) + str(total_proc[1])
            file.close()

        except:
            return 'NI! NI! NI! [ Monty Python ]'

        processor_load_t2 = float(processor_load_all)

        try:
            file = open('/proc/' + str(pid) + '/stat')
            pid_proc = file.read().split()
            processor_load_pid = int(pid_proc[13]) + int(pid_proc[14])
            file.close()

        except:
            return 'We are now no longer the Knights who say NI! [ Monty Python ]'

        pid_load_t2 = float(processor_load_pid)

        try:
            cpu = (((pid_load_t2 - pid_load_t1) * 100) / (processor_load_t2 - processor_load_t1))

        except:
            cpu = 0

        return str("%2.2f" % cpu) + '%'


    def __init__(self, freq, cpumem, pid):
        """__init__"""

        self.freq     = freq
        self.cpumem   = cpumem
        self.pid      = pid
        self.procname = self.get_info_pid(self.pid)

        if 'm' in self.cpumem:
            self.m = self.ram_pid(self.pid)

        if 'c' in self.cpumem:
            self.p = self.proc_pid(self.pid, 0.1)
        else:
            try:
                sleep(0.1)
            except:
                return 'Ahhhh'

        self.counter = 0

        threading.Thread.__init__ (self)


    def run(self):

        while 1:
            if 'm' in self.cpumem:
                self.m = self.ram_pid(self.pid)

            if 'c' in self.cpumem:
                self.p = self.proc_pid(self.pid, self.freq)
            else:
                try:
                    sleep(self.freq)
                except:
                    return 'Ahhhh'

            self.counter += self.freq


    def info(self):

        print('')

        print(self.procname, '-', str(self.pid))
        print('Frequency:\t', str(self.freq) + 's')
        print('Counter:\t', str(self.counter) + 's')

        if 'm' in self.cpumem:
            print('Memory:\t\t', self.m)

        if 'c' in self.cpumem:
            print('Processor:\t', self.p)
