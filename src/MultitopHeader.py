# TODO: write here about this file

import threading
from time import sleep

class MultitopHeader(threading.Thread):
    '''Header class'''
    def __init__(self):
        self.up = self.uptime()
        self.load = self.loadavg()
        self.mem = self.meminfo()
        self.cpu = self.cpuinfo(0.1)
        self.mem_percent = self.mem_percent_info()

        threading.Thread.__init__ (self)


    def run(self):
        while 1:
            self.up = self.uptime()
            self.load = self.loadavg()
            self.mem = self.meminfo()
            self.cpu = self.cpuinfo(1)
            self.mem_percent = self.mem_percent_info()


    def uptime(self):

        # /proc/uptime
        # This file contains two numbers:
        # - The uptime of the  system  (seconds)
        # - The amount of time spent in idle process (seconds)

        try:
            file = open('/proc/uptime')
            uptime = file.read().split()
            file.close()

        except:
            return 'Are you suggesting coconuts migrate? [ Monty Python ]'

        seconds_up = float(uptime[0])

        minute = 60           #    60 seconds
        hour   = minute * 60  #  3600 seconds
        day    = hour * 24    # 86400 seconds

        days     = int (seconds_up / day)
        hours    = int ((seconds_up % day) / hour)
        minutes  = int ((seconds_up % hour) / minute)
        seconds  = int (seconds_up % minute)

        if days > 0:
            return str(str(days) + 'd:' + str(hours) + 'h:' + str(minutes) + 'm:' + str(seconds) + 's')
        if hours > 0:
            return str(str(hours) + 'h:' + str(minutes) + 'm:' + str(seconds) + 's')
        if minutes > 0:
            return str(str(minutes) + 'm:' + str(seconds) + 's')
        if seconds > 0:
            return str(str(seconds) + 's')


    def loadavg(self):

        # /proc/loadavg
        # The first three fields in this file are load average figures giving the number of jobs in the
        # run queue (state R) or waiting for disk I/O (state D) averaged over 1,  5, and 15 minutes.
        # The fifth field is the PID of the process that was most recently created on the system.

        try:
            file = open('/proc/loadavg')
            loadavg = file.read().split()
            file.close()

        except:
            return 'None shall pass! [ Monty Python ]'

        return str(str((loadavg[0])) + ' ' + str((loadavg[1])) + ' ' + str((loadavg[2])))


    def meminfo(self):

        # /proc/meminfo
        # This is used by free(1) to report the amount of free and used memory (both physical and swap)
        # on the system as well as the shared memory and buffers used by  the kernel.
        # It is in the same format as free(1).

        try:
            file = open('/proc/meminfo')
            meminfo = file.read().split()
            file.close()

        except:
            return 'I move for no man! [ Monty Python ]'

        conversion_factor = 1024 # 1024KB -> 1 MB

        mem_total   = (int(meminfo[1]) / conversion_factor)
        mem_free    = (int(meminfo[4]) / conversion_factor)
        mem_buffers = (int(meminfo[7]) / conversion_factor)
        mem_cached  = (int(meminfo[10]) / conversion_factor)
        mem_used    = ((int(meminfo[1]) - int(meminfo[4])) / conversion_factor)

        swap_total  = (int(meminfo[34]) / conversion_factor)
        swap_free   = (int(meminfo[37]) / conversion_factor)
        swap_used   = ((int(meminfo[34]) - int(meminfo[37])) / conversion_factor)

        # strReturn = str('RAM[mb]: ' + str(mem_total) + ' Total, ' + str(mem_free) + ' Free, ' + str(mem_buffers) + ' Buffers, ' + str(mem_cached) + ' Cached, ' + str(mem_used) + ' Used\n')
        strReturn = str('\tRAM: ' + str(mem_total) + ' Total, ' + str(mem_free) + ' Free, ' + str(mem_used) + ' Used\t[MB]\n')
        strReturn += str('\tSWAP: ' + str(swap_total) + ' Total, ' + str(swap_free) + ' Free, ' + str(swap_used) + ' Used\t[MB]')

        return strReturn


    def mem_percent_info(self):

        # /proc/meminfo
        # This is used by free(1) to report the amount of free and used memory (both physical and swap)
        # on the system as well as the shared memory and buffers used by  the kernel.
        # It is in the same format as free(1).

        try:
            file = open('/proc/meminfo')
            meminfo = file.read().split()
            file.close()

        except:
            return 'I move for no man! [ Monty Python ]'

        conversion_factor = 1024 # 1024KB -> 1 MB

        mem_total   = (float(meminfo[1]) / conversion_factor)
        mem_used    = ((float(meminfo[1]) - float(meminfo[4])) / conversion_factor)

        swap_total  = (float(meminfo[34]) / conversion_factor)
        swap_used   = ((float(meminfo[34]) - float(meminfo[37])) / conversion_factor)


        used_percent_mem = ((mem_used * 100) / mem_total)
        used_percent_swap = ((swap_used * 100) / swap_total)


        return str("%2.2f" % used_percent_mem) + '% RAM - ' + str("%2.2f" % used_percent_swap) + '% SWAP'


    def cpuinfo(self, tts):

        try:
            file = open('/proc/stat')
            total_proc = file.read().split(' ')
            processor_load_all = str(total_proc[5])
            file.close()
        except:
            return 'What else floats in water? A Duck! [ Monty Python ]'

        processor_load_idle_1 = float(processor_load_all)

        try:
            file = open('/proc/stat')
            pid_proc = file.read().split(' ')
            processor_load_pid = 0

            i = 2
            while i <= 9:
                processor_load_pid = processor_load_pid + int(pid_proc[i])
                i += 1

            file.close()

        except:
            return 'We are the Knights who say... NI! [ Monty Python ]'

        processor_load_total_1 = float(processor_load_pid)


        try:
            sleep(tts)
        except:
            return 'Blue.. No. Yellow. Aaaaaaahh'


        try:
            file = open('/proc/stat')
            total_proc = file.read().split(' ')
            processor_load_all = str(total_proc[5])
            file.close()
        except:
            return 'What else floats in water? A Duck! [ Monty Python ]'

        processor_load_idle_2 = float(processor_load_all)

        try:
            file = open('/proc/stat')
            pid_proc = file.read().split(' ')
            processor_load_pid = 0

            i = 2
            while i <= 9:
                processor_load_pid = processor_load_pid + int(pid_proc[i])
                i += 1

            file.close()

        except:
            return 'We are the Knights who say... NI! [ Monty Python ]'

        processor_load_total_2 = float(processor_load_pid)

        diff_idle = (processor_load_idle_2 - processor_load_idle_1)
        diff_total = (processor_load_total_2 - processor_load_total_1)

        try:
            system_cpu = (((diff_total - diff_idle) / diff_total) * 100)

        except:
            system_cpu = 0

        return str("%2.2f" % system_cpu) + '%'


    def info(self):

        print('')
        print(62 * '-')
        print('UPTIME: ' + self.up + '\t\tLOAD AVERAGE: ' + self.load)


        print(62 * '-')
        print('CPU USAGE: ' + self.cpu + '\tMEM USAGE: ' + self.mem_percent)


        print(62 * '-')
        print('MEMORY USAGE:\n\n' + self.mem)
        print(62 * '-')
        print('')
