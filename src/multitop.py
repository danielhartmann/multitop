#!/usr/bin/env python
# -*- coding: utf-8 -*-
# TODO: write here about this file

__author__ = "Daniel Hartmann [daniel.hnn@gmail.com] - Fernando Bohrer [fernandobohrer@gmail.com] - Janaina Bald [janainabald@gmail.com]"
__date__ = "$17/03/2009 15:18:19$"

import os
import sys
from time import sleep

from MultitopCore import Multitop
from MultitopHeader import MultitopHeader

## Script session ##
if __name__ == '__main__':

    # checking arguments
    args = sys.argv
    lenArgs = len(args)
    usage = '\nUsage: ' + args[0] + ' period-of-update1 kind1[m/c/mc/cm] pid1 ... period-of-updateN kindN[m/c/mc/cm] pidN \n'

    if lenArgs == 1:
        print('\n' + args[0] + ': Bad period of update\n')
        sys.exit(0)

    if args[1] == '--help':
        print(usage)
        sys.exit(0)

    if lenArgs == 2:
        print('\n' + args[0] + ': Bad kind of information\n')
        sys.exit(0)


    # verificacao de parametros - m|c

    if (args[2] == 'm'):
        #print( 'Ativar flag memoria' )
        pass
    elif (args[2] == 'c'):
        #print( 'Ativar flag cpu' )
        pass
    elif (args[2] == 'mc'):
        #print( 'Ativar flags memoria e cpu' )
        pass
    elif (args[2] == 'cm'):
        #print( 'Ativar flags cpu e memoria' )
        pass
    else:
        print('\n' + args[0] + ': Bad kind of information\n')
        sys.exit(0)


    if lenArgs == 3:
        print('\n' + args[0] + ': Bad pid\n')
        sys.exit(0)


    groupedArgs = []
    aux = []
    i = 0
    j = 0
    for arg in sys.argv[1:]:
        aux.append(arg)
        i += 1

        if i == 3:
            groupedArgs.append(aux)
            j += 1
            i = 0
            aux = []

    if (lenArgs - 1) % 3 != 0:
        print(args[0] + ': faltam operandos')
        sys.exit(0)

    instances = []

    instance = MultitopHeader()
    instance.start()
    instances.append(instance)

    # loop through arguments
    for group in groupedArgs:
        freq   = group[0]
        # check if frequency is an integer
        try:
            freq = int(freq)
        except:
            print('Valor do periodo invalido. Deve ser um inteiro.')
            sys.exit(0)

        cpumem = group[1]
        pid    = group[2]
        # check if proccess id is an integer
        try:
            freq = int(freq)
        except:
            print('Valor do periodo invalido. Deve ser um inteiro.')
            sys.exit(0)

        instance = Multitop(freq, cpumem, pid)
        instance.start()
        instances.append(instance)


    counter = 0

    while 1:

        os.system('clear')

        print('')
        print(21 * ' ' + '--|-- MULTITOP --|--')


        for instance in instances:
            instance.info()

        try:
            sleep(1)

        except:
            sys.exit(0)

    sys.exit(0)
